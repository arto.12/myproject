import {BrowserRouter, Route, Routes} from "react-router-dom";
import {Navbar} from "./components/Navbar.js";
import {Header} from "./components/Header.js";
import Home from "./components/Home.js";
import Series from "./components/Series.js";
import Movies from "./components/Movies.js";
import {Footer} from "./components/Footer.js";
import './styles/style.css';
import './styles/responsive.css';

function App() {
  return (
    <BrowserRouter>
        <Navbar />
        <Header />
        <Routes>
          <Route path="/" element={<Home/>}/>
          <Route path="/series" element={<Series/>}/>
          <Route path="/movies" element={<Movies/>}/>
        </Routes>
        <Footer />
    </BrowserRouter>
  );
}

export default App;
