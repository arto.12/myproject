import React, {useState, useEffect} from "react";
import axios from 'axios'
import {Link} from "react-router-dom";

export const Movies = () => {

    const renderCount = []

    const arrAdd = () => { renderCount.push("1") }

    const getUrl = (obj) => {
        for (const key in obj) {
            return (obj[key].url)
        }
    }

    const [data, setData] = useState('');
    const [loadin, setLoading] = useState(false);
    const [error, setError] = useState(false);

    const fetchData = async () => {
        try {
            const data = await axios
                .get('/feed.json')
                .then((res) => {
                    res.data.entries.sort(function(a, b){
                        if(a.title < b.title) { return -1; }
                        if(a.title > b.title) { return 1; }
                        return 0;
                    })
                    setData(res.data)
                });
            setLoading(true)
        } catch (e) {
            setError(true)
        }
    }

    useEffect(() => {
        fetchData()
        return () => {
            setData('')
        }
    }, [])

    return (
        <div className="main container">
            <div className="main-content">
                <div className="navigation">
                    <Link to="/">
                        <div className="back"><i className="fas fa-arrow-left"></i></div>
                    </Link>
                </div>
                {error ? <div className="message">Oops something went wrong...</div> :
                loadin ? ( data && data.entries.map(((item, idx) => {
                    return (
                        item.programType === 'movie' && item.releaseYear >= 2010 && renderCount.length < 21 && <div className="serial-item" key={idx}>
                            {}
                            <div className="item-img">
                                <img src={getUrl(item.images)} alt=""/>
                            </div>
                            {arrAdd()}
                            <span><strong>{item.title}</strong><br/><small>{item.releaseYear}</small></span>
                        </div>
                    )
                }))) : (<div className="message">Loading...</div>)
                }
            </div>
        </div>
    )
}

export default Movies