import React from "react";

export const Header = () => {
    return (
        <div className="header">
            <div className="header-content container">
                <div className="header-title">
                    <p>popular</p>
                    <p>titles</p>
                </div>
                    <div className="nav-info log-inf">
                        <p>log in</p>
                        <p>start your free trial</p>
                    </div>
            </div>
        </div>
    )
}
