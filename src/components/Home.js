import React from "react";
import {Link} from "react-router-dom";
import  placeholder  from "../img/placeholder.png"

export const Home = () => {
    return (
        <div className="main container">
            <div className="navigation">
            </div>
            <div className="home main-content">
                <Link to="/series">
                    <div className="serials">
                        <span className="series-title">series</span>
                        <div>
                            <img src={placeholder} alt=""/>
                        </div>
                    </div>
                    <span className="i-f">popular series</span>
                </Link>

                <Link to="/movies">
                    <div className="films">
                        <span className="films-title">films</span>
                        <div>
                            <img src={placeholder} alt=""/>
                        </div>
                    </div>
                    <span className="i-f">popular movies</span>
                </Link>
            </div>
        </div>
    )
}

export default Home